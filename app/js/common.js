$(function() {
  // Custom JS
  'use strict';
  var owl = $(".owl-carousel");
  owl.owlCarousel({
    loop: true,
    // margin: 25,
    margin: 5,
    nav: false,
    dots: false,
    navText: "",
    responsive: {
      0: {
        items: 1,
      },
      480: {
        items: 2,
      },
      768: {
        items: 3,
      }
    }
  });
  $("#next").click(function() {
    owl.trigger('next.owl.carousel');
  });
  $("#prev").click(function() {
    owl.trigger('prev.owl.carousel');
  });
  // Does the browser actually support the video element?
  var supportsVideo = !!document.createElement('video').canPlayType;

  if (supportsVideo) {
    // Obtain handles to main elements
    var video = document.getElementById('video');

    // Hide the default controls
    video.controls = false;


    // Obtain handles to buttons and other elements
    var playpause = document.getElementById('playpause');
    var overlay = document.getElementById('video-overlay');

    // Only add the events if addEventListener is supported (IE8 and less don't support it, but that will use Flash anyway)
    if (document.addEventListener) {
      // Add events for all buttons
      playpause.addEventListener('click', function(e) {
        if (video.paused || video.ended) {
          video.play();
          playpause.className += ' hidden';
          overlay.className += ' hidden';
          video.controls = true;
        } else video.pause();
      });
    }
  }

  $('a[data-rel^=lightcase]').lightcase({
    showSequenceInfo: false,
    showCaption: false,
    swipe: true,
    labels: {
      errorMessage: 'Не удалось загрузить изображение...'
    },
  });

  var waypoint = new Waypoint({
    element: document.getElementById('positive-reviews'),
    handler: function() {
      $('.progress .progress-bar').css("width",
        function() {
          return $(this).attr("aria-valuenow") + "%";
        }
      )
    },
    offset: '100%'
  })

});
